RightClickBrowser
=================

Open links in another browser via the right-click menu

## How to Build

Requires Visual Studio 2019

 1. Open "Developer Command Prompt" on Windows
 2. cd host
 3. nmake

## License

MIT License
