chrome.contextMenus.create({
    id: "right-click-browser",
    title: "Firefoxで開く",
    contexts: ["link"],
});

chrome.contextMenus.onClicked.addListener(function(info, tab) {
    switch(info.menuItemId) {
    case "right-click-browser":
        openFirefox(info.linkUrl);
        break;
    }
});

function openFirefox(url) {
    chrome.runtime.sendNativeMessage('com.clear_code.rightclickbrowser', new String("F " + url));
}
