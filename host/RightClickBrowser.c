#include <Windows.h>
#include <io.h>
#include <fcntl.h>
#include <stdio.h>

#define VERSION "RightClickBrowser 1.0"

/*
 * Launch firefox via ShellExecute()
 */
static int open_firefox(const char *url)
{
    HINSTANCE h;

    h = ShellExecuteA(NULL, "open", "firefox.exe", url, NULL, SW_SHOW);
    if ((int) h < 33) {
        fprintf(stderr, "cannot launch firefox url=%s", url);
        return -1;
    }
    return 0;
}

/*
 * Implement Native Messaging Protocol.
 */
static void do_response(const char *msg, ...)
{
    va_list args;
    int len;

    /* Prevent Windows from translating CRLF */
    setmode(_fileno(stdout), O_BINARY);

    // Print 4-byte header
    va_start(args, msg);
    len = vsnprintf(NULL, 0, msg, args);
    fwrite(&len, sizeof(len), 1, stdout);
    va_end(args);

    // Push the remaining body to stdout
    va_start(args, msg);
    vfprintf(stdout, msg, args);
    fflush(stdout);
    va_end(args);
}

static int handle_request(void)
{
    int len;
    int ret = -1;
    char *buf = NULL;
    char *url;
    char browser;

    /* Read Native Messaging string */
    if (fread(&len, sizeof(len), 1, stdin) < 1) {
        fprintf(stderr, "cannot read %i bytes", sizeof(len));
        return -1;
    }

    /* The shortest input is "F http://" (including quotes) */
    if (len < 11) {
        return -1;
    }

    buf = (char *) calloc(1, len + 1);
    if (buf == NULL) {
        perror("calloc");
        return -1;
    }

    if (fread(buf, len, 1, stdin) < 1) {
        fprintf(stderr, "cannot read %i bytes", len);
        free(buf);
        return -1;
    }

    /*
     * "F https://www.clear-code.com"
     *  - --------------------------
     */
    buf[len - 1] = '\0';
    browser = buf[1];
    url = buf + 3;

    switch (browser) {
    case 'F':
        ret = open_firefox(url);
        break;
    default:
        break;
    }
    free(buf);
    return ret;
}

int main(int argc, char *argv[])
{
    if (argc > 1) {
        if (strcmp(argv[1], "--version") == 0) {
            printf("%s\n", VERSION);
            return 0;

        }
        else if (strcmp(argv[1], "--firefox") == 0) {
            return open_firefox("https://www.example.com/");
        }
    }
    return handle_request();
}
