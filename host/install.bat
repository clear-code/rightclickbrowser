@echo off
setlocal
cd %~dp0

REG ADD HKLM\Software\Microsoft\Edge\NativeMessagingHosts\com.clear_code.rightclickbrowser /f /t REG_SZ /ve /d "%~dp0\edge.json" /reg:32
